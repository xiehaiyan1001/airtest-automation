from poco.exceptions import PocoTargetTimeout
from business.login_business import *
from airtest.core.api import *
# from handle.login_handle import LoginPage, MainPage
import pytest
from common.logger import Logger
import allure
# from common.read_yaml import ReadYaml
# airtest_data = ReadYaml(r"airtest_data.yml").get_yaml_data()#读取数据
# jiakaobaodian  = airtest_data['jiakaobaodian']

poco = MainPage().poco

def air_assert(loc,doc=""):
    file_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)) + "/png/{}_{}.png".format(doc,time.strftime("%y%m%d-%H%M%S",
                                                                                                         time.localtime())))
    try:
        pytest.assume(poco(text=loc).exists() == True)
        poco(text=loc).wait_for_appearance(timeout=5)
    except PocoTargetTimeout:
        snapshot(filename=file_path, msg="{}".format(doc))
        Logger.error('=====>{}校验用例-------用例执行未通过'.format(doc))
    Logger.info("***************{}校验用例-----结束执行用例 ***************".format(doc))