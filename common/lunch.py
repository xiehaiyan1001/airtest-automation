from airtest.core.api import connect_device, start_app
from airtest.utils.logger import *
from common.main_page import MainPage
import os
import yaml
import time

class APP:
    config = yaml.safe_load(open(os.path.abspath(os.path.dirname(os.path.dirname(__file__)) +'config/device.yaml'),'r',encoding='utf-8'))["Android"]

    @classmethod
    def start(cls):
        connect_device("Android://127.0.0.1:5037/{}".format(cls.config['deviceName']))
        start_app(cls.config['appPackage'])
        # logger = get_logger("airtest")
        # logger.setLevel(logging.INFO)
        return  MainPage

    @classmethod
    def quit(cls):
        time.sleep(5)
        # stop_app(cls.config['appPackage'])
