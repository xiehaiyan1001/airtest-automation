# -*- encoding=utf8 -*-
from poco.exceptions import PocoTargetTimeout

__author__ = "hvnb"

from airtest.core.api import *

auto_setup(__file__)


from poco.drivers.android.uiautomation import AndroidUiautomationPoco
poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)

# pip install -i https://pypi.tuna.tsinghua.edu.cn/simple/ 库名
# 点击
poco(desc = "驾考宝典").click()