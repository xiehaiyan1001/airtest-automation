from poco.exceptions import PocoTargetTimeout
from business.login_business import *
from airtest.core.api import *
# from handle.login_handle import LoginPage, MainPage
import pytest
from common.logger import *
import allure
# from common.read_yaml import ReadYaml
from common.air_assert import air_assert
# airtest_data = ReadYaml(r"airtest_data.yml").get_yaml_data()#读取数据
# jiakaobaodian  = airtest_data['jiakaobaodian']

poco = MainPage().poco

@allure.feature('驾考宝典')#测试报告显示测试功能
# @pytest.mark.usefixtures("start_atop")#前后置打开和关闭app
class TestLogin:

    @allure.title("报名页面驾考排行校验")#标题
    @allure.step('打开驾考宝典-点击报名')#测试报告显示步骤
    @decorate_log
    def test_login_on(self):
        Logger.info("***************开始执行用例 报名页面驾考排行校验用例***************")
        # page = LoginPage()
        business =LoginBusiness()
        business.jiakao()
        # jiakao1(self)
        air_assert(wanfeng,doc="报名页面驾考排行")

if __name__=="__main__":
    pytest.main(['-s',__file__])