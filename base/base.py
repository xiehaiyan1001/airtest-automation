# -*- encoding=utf8 -*-
from poco.exceptions import PocoTargetTimeout
from common.logger import Logger

__author__ = "hvnb"

from airtest.core.api import *

auto_setup(__file__)


from poco.drivers.android.uiautomation import AndroidUiautomationPoco
poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)


def click_hbq(locator):
	return poco(text=locator).click()


class Test_Api():
	# 截图操作
	def save_screenshot(self, doc):
		"""  知识点解析      #time strftime() 函数接收以时间元组，并返回以可读字符串表示的当地时间，格式由参数 format 决定。"""
		# 图片名称  页面名称  操作名称   时间  png
		file_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)) + "/png/{}_{}.png".format(doc,
																										 time.strftime(
																											 "%y%m%d-%H%M%S",
																											 time.localtime())))
		try:
			snapshot(filename=file_path, msg="{}".format(doc))
			Logger.info("截图成功，文件路径:{}".format(file_path))
		except:
			Logger.exception("{}-截图失败！！！".format(doc))
	# 点击操作fffffff
	def click(self, locator, doc=""):
		try:
			click = poco(text=locator).click()
			Logger.info("{}-元素{}点击成功".format(doc, locator))
		except:
			Logger.exception("{}-元素点击失败-{}".format(doc, locator))
			self.save_screenshot("{}-点击失败".format(doc))
			raise
	# 输入操作
	def text(self, locator,text, doc=""):
		try:
			click = poco(text=locator).set_text(text)
			Logger.info("{}-元素{}输入成功".format(doc, locator))
		except:
			Logger.exception("{}-元素输入失败-{}".format(doc, locator))
			self.save_screenshot("{}-输入失败".format(doc))
			raise
	# #点击
	# def click(self,poco1):
	# 	print("11111111111111111",poco1)
	# 	poco(poco1).click()
	#
	# #输入
	# def text(self,poco1,text):
	# 	return poco(poco1).set_text(text)
	#
	# # 对象存在判断
	# def exists(self,poco1):
	# 	invisible_obj = poco(poco1)
	# 	invisible_obj.exists()

	# 拖动和滑动（drag/swipe）
	def drag(self,poco_start,poco_stop):
		poco(poco_start).drag_to(poco(poco_stop))
